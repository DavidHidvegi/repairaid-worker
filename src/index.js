import { containerBootstrap } from "@nlpjs/core";
import { Nlp } from "@nlpjs/nlp";
import { LangEn } from "@nlpjs/lang-en-min";

export default {
	async fetch(request, env, ctx) {
		
		const allowedOrigins = [
			"https://repairaid.davidhidvegi.com",
			"https://repairaid.pages.dev",
			"http://localhost:3000"
		]

		const corsHeaders = origin => ({
			'Access-Control-Allow-Headers': '*',
			'Access-Control-Allow-Methods': 'POST',
			'Access-Control-Allow-Origin': origin
		})

		const resolveQuery = async request => {
			const { query } = await request.json()

			console.log(query) // Log incoming query 

			const container = await containerBootstrap()
			container.use(Nlp)
			container.use(LangEn)
			const nlp = container.get('nlp');
			nlp.settings.autoSave = false;
			nlp.addLanguage('en');
			
			nlp.addDocument('en', 'hello', 'greetings.hello');
			nlp.addDocument('en', 'hi', 'greetings.hello');
			nlp.addDocument('en', 'howdy', 'greetings.hello');
			nlp.addDocument('en', 'heyho', 'greetings.hello');
			nlp.addAnswer('en', 'greetings.hello', 'Hey there!');
			nlp.addAnswer('en', 'greetings.hello', 'Greetings!');

			nlp.addDocument('en', 'goodbye for now', 'greetings.bye');
			nlp.addDocument('en', 'bye bye take care', 'greetings.bye');
			nlp.addDocument('en', 'okay see you later', 'greetings.bye');
			nlp.addDocument('en', 'bye for now', 'greetings.bye');
			nlp.addDocument('en', 'i must go', 'greetings.bye');
			nlp.addAnswer('en', 'greetings.bye', 'Till next time');
			nlp.addAnswer('en', 'greetings.bye', 'see you soon!');

			await nlp.train();
			const answer = await nlp.process('en', query);
		
			const allowedOrigin = checkOrigin(request)
			return new Response(JSON.stringify(answer), {
				headers: {
					'Content-type': 'application/json',
					...corsHeaders(allowedOrigin)
				}
			})
		}

		const checkOrigin = request => {
			const origin = request.headers.get("Origin")
			const foundOrigin = allowedOrigins.find(allowedOrigins => allowedOrigins.includes(origin))
			return foundOrigin ? foundOrigin : allowedOrigins[0]
		}

		if (request.method === 'OPTIONS'){
			const allowedOrigin = checkOrigin(request)
			return new Response("OK", { headers: corsHeaders(allowedOrigin) })
		} else

		if (request.method === "POST") {
			return resolveQuery(request)
		}
	},
};
